## KupferDigital Core Ontology

[![OntoFlow](https://gitlab.com/kupferdigital/ontologies/core/badges/main/pipeline.svg?style=flat-square&ignore_skipped=true&key_text=OntoFlow&key_width=80)](https://kupferdigital.gitlab.io/ontologies/core)
[![Graph](https://gitlab.com/kupferdigital/ontologies/core/badges/main/pipeline.svg?style=flat-square&ignore_skipped=true&key_text=Turtle&key_width=80)](https://kupferdigital.gitlab.io/ontologies/core/index.ttl)

<img src="https://kupferdigital.gitlab.io/ontologies/core/core.svg">
### draw.io and Chowlk

- [Issue](https://github.com/oeg-upm/Chowlk/issues/53): Adding links to elements will break the Chowlk export, even after removing the link.

### Style Guide

First page `ontology` contains the ontology, further pages can be added to show ho to apply the ontology to model specific things.

#### Ontology

- `kd:` namespace is implicitly assumed and should be left out.
- Font size should be **12pt** for elements.
- An Element's **width** should be a multiple of **40pt**, but not wider than necessary to display its content.
- An Element's **height** should be a multiple of **30pt**.
- Elements should snap to the big grid. Snap to half of the full grid is allowed, to keep a vertical error straight.
- When you are positing an Element, blue snap lines appear, which align the moved element to other existing elements. If a snap appears and the popup box which indicates the position of the grabbed element does not show a nice round number dividable by 10. Find the source of the snap and correct it.
- When using a term like `bfo:BFO_0000002` add a text element close by with **10pt** *cursive* font.
- Boxes for aligning with BFO/IOF should be light blue.
- Arrows should always be attached to the center to the box and not the edge. When selected their ends should be indicated by a blue arrow.

#### Modeling Examples

- Elements which a reasoner would add should be **gray**.

### Used Prefixes

```text
bfo: http://purl.obolibrary.org/obo/
chebi: http://purl.obolibrary.org/obo/chebi#
dc: http://purl.org/dc/terms/
dx: https://dx.doi.org/10.31030/
iof: https://purl.industrialontologies.org/ontology/core/Core/
kd: https://kupferdigital.gitlab.io/ontologies/kupferdigital#
owl: http://www.w3.org/2002/07/owl#
qudt: http://qudt.org/schema/qudt/
qudtk: http://qudt.org/vocab/quantitykind/
rdfs: http://www.w3.org/2000/01/rdf-schema#
sto: https://w3id.org/i40/sto#
wd: https://www.wikidata.org/wiki/
```

#### QUDT

- [Documentation](https://www.qudt.org/pages/HomePage.html)
